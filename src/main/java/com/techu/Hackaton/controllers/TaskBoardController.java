package com.techu.Hackaton.controllers;


import com.techu.Hackaton.models.TaskBoardModel;
import com.techu.Hackaton.services.TaskBoardService;
import com.techu.Hackaton.services.TaskBoardServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/taskboard")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE })
public class TaskBoardController {

    @Autowired
    TaskBoardService taskBoardService;
    @Autowired
    TaskBoardServiceResponse taskBoardServiceResponse;

    @GetMapping("/task")
    public ResponseEntity<List<TaskBoardModel>> getTask(@RequestParam( name = "$orderby" , required = false) String orderBy){
        System.out.println("getTask");

        System.out.println(orderBy);
        if(orderBy != null){
            System.out.println("getTask Sort");
            return new ResponseEntity<>(
                    this.taskBoardService.findAllOrder(orderBy),
                    HttpStatus.OK);
        }else{
            return new ResponseEntity<>(
                    this.taskBoardService.findAll(),
                    HttpStatus.OK);
        }

    }

    @GetMapping("/task/{id}")
    public ResponseEntity<Object> getTaskById(@PathVariable String id){
        System.out.println("getTaskById");
        System.out.println(" ID " + id);

        Optional<TaskBoardModel> result = this.taskBoardService.findID(id);


        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "compra no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }
    
     @PostMapping("/task")
    public ResponseEntity<Object> addTask(@RequestBody TaskBoardModel task){
        System.out.println("addTask");

         TaskBoardServiceResponse taskBoardServiceResponse = this.taskBoardService.addTask(task);

         return new ResponseEntity<>(
                 taskBoardServiceResponse.getResponseHttpStatusCode().equals(HttpStatus.BAD_REQUEST) ? taskBoardServiceResponse.getMsg() : taskBoardServiceResponse.getTaskModel(),
                 taskBoardServiceResponse.getResponseHttpStatusCode());
    }




    @PutMapping("/task/{id}")
    public ResponseEntity<Object> updateTask(@PathVariable String id, @RequestBody TaskBoardModel task){
        System.out.println("updateUser");
        System.out.println("URL ID " + id);
        System.out.println("BODY ID " + task.getTaskId());
        boolean actualizado = false;

        Optional<TaskBoardModel> result = this.taskBoardService.findID(id);


        if(result.isPresent() && id.equals(task.getTaskId())){
            this.taskBoardServiceResponse = this.taskBoardService.update(task);

        }else{
            this.taskBoardServiceResponse.setMsg("La tarea no se encuentra");
            this.taskBoardServiceResponse.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(
                this.taskBoardServiceResponse.getResponseHttpStatusCode().equals(HttpStatus.OK) ? taskBoardServiceResponse.getTaskModel() : taskBoardServiceResponse.getMsg() ,
                this.taskBoardServiceResponse.getResponseHttpStatusCode());
    }

    @DeleteMapping("/task/{id}")
    public ResponseEntity<Object> DelpurchaseById(@PathVariable String id){
        System.out.println("DelpurchasesById");
        System.out.println(" ID " + id);
        boolean eliminado = false;

        Optional<TaskBoardModel> result = this.taskBoardService.findID(id);

        if (result.isPresent()) {

            this.taskBoardService.delete(id);
            this.taskBoardServiceResponse.setMsg("tarea eliminada");
            this.taskBoardServiceResponse.setResponseHttpStatusCode(HttpStatus.OK);
        } else {
            this.taskBoardServiceResponse.setMsg("tarea no encontrada");
            this.taskBoardServiceResponse.setResponseHttpStatusCode(HttpStatus.NOT_FOUND);
        }


        return new ResponseEntity<>(
                this.taskBoardServiceResponse.getMsg(),
                this.taskBoardServiceResponse.getResponseHttpStatusCode());
    }


}
