package com.techu.Hackaton.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document(collection = "columns")
public class ColumnModel {

    @Id
    private String columnId;
    private String nombre;
    private String position;
}
