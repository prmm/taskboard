package com.techu.Hackaton.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "task")
public class TaskBoardModel {

    @Id
    private String taskId;
    private String taskName;
    private String color;
    private String description;
    private String columnId;
    private String position;

    public TaskBoardModel() {
    }

    public TaskBoardModel(String id, String taskName, String color, String description, String columnId, String position) {
        this.taskId = id;
        this.taskName = taskName;
        this.color = color;
        this.description = description;
        this.columnId = columnId;
        this.position = position;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColumnId() {
        return columnId;
    }

    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
