package com.techu.Hackaton.services;

import com.techu.Hackaton.models.TaskBoardModel;
import com.techu.Hackaton.repositories.TaskBoardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class TaskBoardService {

        @Autowired
        TaskBoardRepository purchaseRepository;

        @Autowired
        ColumnService productService;

        public List<TaskBoardModel> findAll(){
            System.out.println("findAll en TaskBoardService)");

            return purchaseRepository.findAll();
        }

    public Optional<TaskBoardModel> findID(String id){
        System.out.println("Buscar por ID en el TaskBoardService");

        return this.purchaseRepository.findById(id);
    }
    public TaskBoardServiceResponse addTask(TaskBoardModel task) {
        System.out.println("addPurchase");

        TaskBoardServiceResponse result = new TaskBoardServiceResponse();

        result.setTask(task);
        result.setResponseHttpStatusCode(HttpStatus.OK);
        ComprobarCampos(result);
        if(result.getResponseHttpStatusCode().equals(HttpStatus.BAD_REQUEST)){
            return result;
        }
        this.purchaseRepository.save(task);
        result.setMsg("Task añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;
    }


    public TaskBoardServiceResponse update(TaskBoardModel task){
        System.out.println("update en el TaskBoardService");

        TaskBoardServiceResponse result = new TaskBoardServiceResponse();

        result.setTask(task);
        result.setResponseHttpStatusCode(HttpStatus.OK);
        ComprobarCampos(result);
        if(result.getResponseHttpStatusCode().equals(HttpStatus.BAD_REQUEST)){
            return result;
        }
        this.purchaseRepository.save(task);
        result.setMsg("Task modificada correctamente");

        return result;

    }

    public void delete(String id){
        System.out.println("Delete by ID");

        this.purchaseRepository.deleteById(id);
    }

    public List<TaskBoardModel> findAllOrder(String order){
        System.out.println("findAll ORDER : "+ order+" en TaskBoardService");

        if(order.equals("ASC")){
            System.out.println("USO EL ASC");
            return purchaseRepository.findAll(Sort.by(Sort.Direction.ASC, "columnId"));
        }else {
            System.out.println("USO EL DESC");
            return purchaseRepository.findAll(Sort.by(Sort.Direction.DESC, "columnId"));
        }
    }




    public TaskBoardServiceResponse ComprobarCampos(TaskBoardServiceResponse taskBoardServiceResponse) {

        if (taskBoardServiceResponse.getTaskModel().getTaskName() == null || taskBoardServiceResponse.getTaskModel().getTaskName().isEmpty() ) {
            System.out.println("Falta Informar el Campo Name");
            taskBoardServiceResponse.setMsg("Falta Informar el Campo Name");
            taskBoardServiceResponse.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
        } else if (taskBoardServiceResponse.getTaskModel().getColor() == null || taskBoardServiceResponse.getTaskModel().getColor().isEmpty()) {
            System.out.println("Falta Informar el Campo Color");
            taskBoardServiceResponse.setMsg("Falta Informar el Campo Color");
            taskBoardServiceResponse.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
        } else if (taskBoardServiceResponse.getTaskModel().getPosition() == null || taskBoardServiceResponse.getTaskModel().getColumnId().isEmpty()) {
            System.out.println("Falta Informar el Campo ColumnId");
            taskBoardServiceResponse.setMsg("Falta Informar el Campo ColumnId");
            taskBoardServiceResponse.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
        } else if (taskBoardServiceResponse.getTaskModel().getPosition() == null || taskBoardServiceResponse.getTaskModel().getDescription().isEmpty()) {
            System.out.println("Falta Informar el Campo Description");
            taskBoardServiceResponse.setMsg("Falta Informar el Campo Description");
            taskBoardServiceResponse.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
        } else if (taskBoardServiceResponse.getTaskModel().getPosition() == null || taskBoardServiceResponse.getTaskModel().getPosition().isEmpty()) {
            System.out.println("Falta Informar el Campo Position");
            taskBoardServiceResponse.setMsg("Falta Informar el Campo Position");
            taskBoardServiceResponse.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
        }
        return taskBoardServiceResponse;
    }


}
