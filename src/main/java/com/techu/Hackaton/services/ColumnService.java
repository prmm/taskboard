package com.techu.Hackaton.services;

import com.techu.Hackaton.models.ColumnModel;
import com.techu.Hackaton.repositories.ColumnRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ColumnService {

        @Autowired
        ColumnRepository productRepository;

        public List<ColumnModel> findAll(){
            System.out.println("findAll en ColumnService)");

            return productRepository.findAll();
        }

        public ColumnModel add(ColumnModel product){
            System.out.println("add en el ColumnService");

            return this.productRepository.save(product);
        }

    public Optional<ColumnModel> findID(String id){
        System.out.println("Buscar por ID en el ColumnService");

        return this.productRepository.findById(id);
    }

    public ColumnModel update(ColumnModel product){
        System.out.println("update en el ColumnService");

        return this.productRepository.save(product);
    }

    public void delete(String id){
        System.out.println("Delete by ID");

        this.productRepository.deleteById(id);
    }

}
