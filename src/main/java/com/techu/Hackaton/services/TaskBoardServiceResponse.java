package com.techu.Hackaton.services;

import com.techu.Hackaton.models.TaskBoardModel;


import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class TaskBoardServiceResponse {

        private String msg;
        private TaskBoardModel taskModel;
        private HttpStatus responseHttpStatusCode;

    public TaskBoardServiceResponse() {
    }

    public TaskBoardServiceResponse(String msg, TaskBoardModel taskBoard, HttpStatus responseHttpStatusCode) {
        this.msg = msg;
        this.taskModel = taskBoard;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public TaskBoardModel getTaskModel() {
        return taskModel;
    }

    public void setTask(TaskBoardModel purchase) {
        this.taskModel = purchase;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}
