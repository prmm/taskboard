package com.techu.Hackaton.repositories;



import com.techu.Hackaton.models.TaskBoardModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskBoardRepository extends MongoRepository<TaskBoardModel, String>{

}
