package com.techu.Hackaton.repositories;


import com.techu.Hackaton.models.ColumnModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColumnRepository extends MongoRepository<ColumnModel, String>{

}
